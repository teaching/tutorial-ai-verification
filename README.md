
URL: https://git.frama-c.com/teaching/tutorial-ai-verification

This repository provide all the necessary material
for the practical session of the course on Verification and Validation of AI.

The main file is a jupyter notebook `code/tutorial.ipynb`, slides of the previous lesson are in `doc/slides.pdf`.

As we use a compiled version of some Python tools, **Python 3.9** or **Python 3.10** should be used. With other version 
you will get an error `bad magic number` making the last part of the tutorial impossible. There are several Python
dependencies to install.

# Getting started
* We suppose python is installed with pip
* First we can move into code folder
  ```
  cd code
  ```
* We recommend creating a dedicated environment for the tutorial, using for instance virtualenv. If virtualenv is 
* not installed run:
  ```
  python -m pip install virtualenv
  ```
  Afterward you can create the virtualenv using:
  ```
  virtualenv ~/.virtualenvs/tuto
  ```
  consider adding `-p /usr/bin/python3.X` pointing to your python version if you have multiple python installed.
  Then activate the environment using 
  ```
  source ~/.virtualenvs/tuto/bin/activate
  ```

* Once in this venv, we can install Python dependencies needed for this tutorial. First install the Jupyter notebook:
  ```
  python -m pip install notebook
  ```

* You can now start the tutorial while installing the rest of the requirements with:
  ```
  python -m pip install -r requirements.txt
  ```
  
* Finally, you can launch the tutorial with:
  ```
  jupyter notebook tutorial.ipynb
  ```

### Authors
- Augustin Lemesle
- François Bobot
- Julien Lehmann
- Serge Durand