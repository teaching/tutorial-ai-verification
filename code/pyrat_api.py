import sys

from loguru import logger

if sys.version_info.minor == 9:
    sys.path.append("pyrat39")
    from pyrat39.src.analyzer.analyzer import analyze, analyze_adv
    from pyrat39.src.analyzer.utils_analyzer import read_inputs, read_model

    from pyrat39.src.analyzer.analysis_param import AnalysisParam
    from pyrat39.src.utils.utils import load_img
    from pyrat39.src.domains.domain_factory import create_box_maker
elif sys.version_info.minor == 10:
    sys.path.append("pyrat310")
    from pyrat310.src.analyzer.analyzer import analyze, analyze_adv
    from pyrat310.src.analyzer.utils_analyzer import read_inputs, read_model

    from pyrat310.src.analyzer.analysis_param import AnalysisParam
    from pyrat310.src.utils.utils import load_img
    from pyrat310.src.domains.domain_factory import create_box_maker
else:
    raise NotImplementedError("Compiled PyRAT only works for Python3.10 or Python3.9.")

import torch

import warnings

warnings.filterwarnings("ignore", message="invalid value encountered in *")
warnings.filterwarnings("ignore", message="divide by zero encountered in reciprocal")


def launch_pyrat(model_path, property_path, domains=[]):
    """
    Evaluate a property on a given neural network.
    Args:
        model_path: path to the model
        property_path: path to the property file
        domains: a python list of domains to propagate in the network from "zonotopes" or "poly"
    """
    logger.remove()

    scorer = "width" if len(domains) == 0 else "coef"
    params = AnalysisParam(
        domains=domains, squeeze=True, verbose=True, check="skip", by_layer=True, split=True, scorer=scorer
    )
    inputs = read_inputs(model_path=model_path, prop_path=property_path, params=params)

    pyrat_model, bounds, to_verify = inputs
    bounds = params.box_maker(bounds[0], bounds[1]).to_type(pyrat_model.dtype)

    result = analyze(bounds, pyrat_model, to_verify, params)
    result.print_res()


def local_robustness(model_path, image, label, pert, domains=[]):
    """
    Verify that the neural network with input x in [image - pert; image + pert] returns the same label.

    Args:
        model_path: path to the model
        image: the image to apply the perturbation to
        label: the desired output label
        pert: the perturbation applied to the image, this perturbation is w.r.t. the infinity norm.
        domains: a python list of domains to propagate in the network from "zonotopes" or "poly"
    Returns:
        Either one of True, "Unknown" or False and the time for the analysis.
    """
    box_maker = create_box_maker("torch", sound=False, dtype=torch.float32)
    params = AnalysisParam(
        total_labels=10,
        epsilon=pert,
        box_maker=box_maker,
        domains=domains,
        by_layer=True,
        force_analysis=True,
        true_label=label,
        check="after",
    )

    pyrat_model = read_model(model_path, params=params)
    res = analyze_adv(image, model_path, pyrat_model, params)

    return res.result, res.time


def read_images():
    images = []
    logger.remove()
    with open("fmnist/img_labels.csv") as f:
        for line in f.readlines():
            name, label = line.split(",")
            image = load_img(f"fmnist/images/{name}", grayscale=False)
            label = int(label.replace("\n", "").strip())
            images.append((image, label))

    return images