FROM python:3.9.18-slim-bullseye

ADD . /home/tutorial/

RUN apt update
RUN apt install gcc -y
RUN /home/tutorial/create_env.sh

